#!/usr/bin/env python3
#
# Copyright 2019 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Test compiler module."""

import pytest
from logic.toolchain.compiler import Arguments


def test_argument_compiler_init():
    """Test arguments."""
    arguments = Arguments()

    assert isinstance(arguments.compiler, str)
    assert arguments.compiler == ''


def test_argument_compiler_set():
    """Test arguments."""
    arguments = Arguments(compiler='verilator')

    assert isinstance(arguments.compiler, str)
    assert arguments.compiler == 'verilator'


def test_argument_compiler_set_none():
    """Test arguments."""
    arguments = Arguments(compiler=None)

    assert isinstance(arguments.compiler, str)
    assert arguments.compiler == ''


def test_argument_compiler_set_error():
    """Test arguments."""
    for argument in [5, 3.2, 3 + 2j, False, True, [], {}, ()]:
        with pytest.raises(TypeError):
            Arguments(compiler=argument)


def test_argument_compiler_dict():
    """Test arguments."""
    arguments = Arguments({'compiler': 'verilator'})

    assert isinstance(arguments.compiler, str)
    assert arguments.compiler == 'verilator'


def test_argument_compiler_dict_none():
    """Test arguments."""
    arguments = Arguments({'compiler': None})

    assert isinstance(arguments.compiler, str)
    assert arguments.compiler == ''


def test_argument_compiler_dict_error():
    """Test arguments."""
    for argument in [5, 3.2, 3 + 2j, False, True, [], {}, ()]:
        with pytest.raises(TypeError):
            Arguments({'compiler': argument})
