#!/usr/bin/env python3
#
# Copyright 2019 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Logic compiler."""

import argparse
import logic


def main():
    """Run command line."""
    parser = argparse.ArgumentParser(
        description='Compiler wrapper for FPGA tools',
        prefix_chars='-+'
    )

    parser.add_argument(
        '--compiler',
        metavar='name',
        default=None,
        dest='compiler',
        help='Compiler name or path to a compiler'
    )

    parser.add_argument(
        '-D',
        metavar='name[=value]',
        action='append',
        default=[],
        dest='defines',
        help='Define'
    )

    parser.add_argument(
        '-I',
        metavar='path',
        action='append',
        default=[],
        dest='includes',
        help='Include'
    )

    parser.add_argument(
        '-L',
        metavar='path',
        action='append',
        default=[],
        dest='paths',
        help='Path'
    )

    parser.add_argument(
        '-l',
        metavar='name',
        action='append',
        default=[],
        dest='libraries',
        help='Library'
    )

    parser.add_argument(
        '-o',
        metavar='output',
        default=None,
        dest='output',
        help='Output'
    )

    parser.add_argument(
        metavar='file',
        nargs='+',
        default=[],
        dest='sources',
        help='Source'
    )

    args, options = parser.parse_known_args()

    arguments = logic.toolchain.compiler.Arguments(
        options=options,
        compiler=args.compiler,
        paths=args.paths,
        outputs=args.output,
        defines=args.defines,
        sources=args.sources,
        includes=args.includes,
        libraries=args.libraries
    )

    compiler = logic.toolchain.compiler.create(args.compiler)

    arguments.compiler_id = compiler.get_id()

    compiler.drive(arguments)


if __name__ == '__main__':
    main()
