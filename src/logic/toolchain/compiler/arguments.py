#!/usr/bin/env python3
#
# Copyright 2019 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Compiler arguments module."""

import os
import json
import typing
import collections


class Arguments:
    """Compiler arguments class."""

    # pylint: disable=too-many-instance-attributes

    ARGUMENTS = [
        'compiler',
        'compiler_id',
        'working_directory',
        'paths',
        'outputs',
        'sources',
        'defines',
        'options',
        'includes',
        'libraries'
    ]

    EXTENDS = [
        'paths',
        'outputs',
        'sources',
        'defines',
        'options',
        'includes',
        'libraries'
    ]

    def __init__(self, *args, **kvargs):
        """Constructor."""
        self._compiler = ''
        self._compiler_id = ''
        self._working_directory = ''
        self._outputs = []
        self._paths = []
        self._sources = []
        self._defines = []
        self._options = []
        self._includes = []
        self._libraries = []

        for arg in args:
            self.load(arg)

        if kvargs:
            self.load(kvargs)

        if not self._working_directory:
            self._working_directory = os.path.abspath(os.getcwd())

    @staticmethod
    def _is_json_file(filename):
        """Check if given filename is a JSON file."""
        return (isinstance(filename, str) and
                os.path.splitext(filename)[1] == '.json')

    @staticmethod
    def _load_json_file(filename):
        """Load arguments from JSON file."""
        if not os.path.exists(filename):
            raise FileNotFoundError('Path doesn\'t exist ' + str(filename))

        if not os.path.isfile(filename):
            raise IsADirectoryError('Path isn\'t a file ' + str(filename))

        arguments = {}

        with open(filename, encoding='utf-8') as json_file:
            arguments = json.load(json_file)

            if not arguments.setdefault('working_directory'):
                working_directory = os.path.dirname(os.path.abspath(json_file))
                arguments['working_directory'] = working_directory

        return arguments

    @staticmethod
    def _dumb_json_file(arguments, filename):
        """Dump arguments to JSON file."""
        with open(filename, mode='w', encoding='utf-8') as json_file:
            json_file.write(json.dumps(arguments))

    @staticmethod
    def _type_string(argument):
        """Return argument as string."""
        if isinstance(argument, str):
            pass
        elif argument is None:
            argument = ''
        else:
            raise TypeError('argument should be a string not a ' +
                            type(argument))
        return argument

    @staticmethod
    def _type_list(argument):
        """Return argument as list."""
        if isinstance(argument, list):
            argument = [item for item in argument if isinstance(item, str)]
        elif isinstance(argument, str):
            argument = [argument]
        elif argument is None:
            argument = []
        else:
            raise TypeError('argument should be a list not a ' +
                            type(argument))
        return argument

    @staticmethod
    def _has_key(args, key: str) -> bool:
        """Check if key exists in arguments."""
        return isinstance(args, collections.abc.Mapping) and (key in args)

    def _normalize(self, working_directory, paths):
        """Return normalized path or paths."""
        if isinstance(paths, str):
            if not os.path.isabs(paths):
                apath = os.path.abspath(os.path.join(working_directory, paths))
                paths = os.path.relpath(apath, self._working_directory)
        elif isinstance(paths, list):
            paths = [self._normalize(working_directory, p) for p in paths]

        return paths

    @staticmethod
    def _get_argument(arguments, key):
        """Get argument by key."""
        argument = None

        if hasattr(arguments, key):
            argument = getattr(arguments, key)
        elif Arguments._has_key(arguments, key):
            argument = arguments[key]

        return argument

    @staticmethod
    def _get_list(arguments, key):
        """Get argument list by key."""
        return Arguments._type_list(Arguments._get_argument(arguments, key))

    @staticmethod
    def _get_working_directory(arguments) -> str:
        """Return working directory."""
        argument = None

        if isinstance(arguments, str):
            argument = arguments
        else:
            argument = Arguments._get_argument(arguments, 'working_directory')

        return Arguments._type_string(argument)

    def _absolute_paths(self, paths):
        """Return absolute path or paths."""
        if isinstance(paths, str):
            if not os.path.isabs(paths):
                paths = os.path.join(self._working_directory, paths)
                paths = os.path.abspath(paths)
        elif isinstance(paths, list):
            paths = [self._absolute_paths(path) for path in paths]

        return paths

    def _relative_paths(self, paths):
        """Return absolute path or paths."""
        if isinstance(paths, str):
            if os.path.isabs(paths):
                paths = os.path.relpath(paths, self._working_directory)
        elif isinstance(paths, list):
            paths = [self._relative_paths(path) for path in paths]

        return paths

    def absolute_paths(self) -> None:
        """Resolve all paths as absolute."""
        self._paths = self._absolute_paths(self._paths)
        self._sources = self._absolute_paths(self._sources)
        self._includes = self._absolute_paths(self._includes)

    def relative_paths(self) -> None:
        """Resolve all paths as relative."""
        self._paths = self._relative_paths(self._paths)
        self._sources = self._relative_paths(self._sources)
        self._includes = self._relative_paths(self._includes)

    def load(self, arguments) -> None:
        """Load arguments."""
        if self._is_json_file(arguments):
            arguments = self._load_json_file(arguments)

        for argument in self.ARGUMENTS:
            value = self._get_argument(arguments, argument)

            if value is not None:
                setattr(self, argument, value)

    def dumb(self, filename=None) -> typing.Dict:
        """Return dictionary and optionaly write to file."""
        arguments = {}

        for argument in self.ARGUMENTS:
            arguments[argument] = getattr(self, argument)

        if filename:
            if self._is_json_file(filename):
                self._dumb_json_file(arguments, filename)
            else:
                raise ValueError("Only JSON files are supported")

        return arguments

    def extend(self, arguments) -> None:
        """Extend existing arguments."""
        if isinstance(arguments, collections.abc.Sequence):
            for argument in arguments:
                self.extend(argument)
        else:
            working_directory = self._get_working_directory(arguments)

            if working_directory is None:
                working_directory = self.working_directory

            for argument in self.EXTENDS:
                value = self._get_list(arguments, argument)

                if argument in ['paths', 'includes', 'sources', 'outputs']:
                    value = self._normalize(working_directory, value)

                getattr(self, argument).extend(value)

    def unique(self):
        """Make it unique."""
        for argument in self.EXTENDS:
            items = Arguments._unique(getattr(self, argument))
            setattr(self, argument, items)

        return self

    @staticmethod
    def _unique(items):
        """Make unique items."""
        unique_items = []

        for item in items:
            if item not in unique_items:
                unique_items.append(item)

        return unique_items

    @property
    def compiler_id(self) -> str:
        """Return compiler id."""
        return self._compiler_id

    @compiler_id.setter
    def compiler_id(self, compiler_id) -> None:
        """Set compiler id."""
        self._compiler_id = self._type_string(compiler_id)

    @property
    def compiler(self) -> str:
        """Return compiler."""
        return self._compiler

    @compiler.setter
    def compiler(self, compiler) -> None:
        """Set compiler."""
        self._compiler = self._type_string(compiler)

    @property
    def working_directory(self) -> str:
        """Return working directory."""
        return self._working_directory

    @working_directory.setter
    def working_directory(self, working_directory) -> None:
        """Set working directory."""
        self._working_directory = self._type_string(working_directory)

    @property
    def paths(self) -> typing.List[str]:
        """Return paths."""
        return self._paths

    @paths.setter
    def paths(self, paths) -> None:
        """Set paths."""
        self._paths = self._type_list(paths)

    @property
    def options(self) -> typing.List[str]:
        """Return options."""
        return self._options

    @options.setter
    def options(self, options) -> None:
        """Set options."""
        self._options = self._type_list(options)

    @property
    def defines(self) -> typing.List[str]:
        """Return defines."""
        return self._defines

    @defines.setter
    def defines(self, defines) -> None:
        """Set defines."""
        self._defines = self._type_list(defines)

    @property
    def sources(self) -> typing.List[str]:
        """Return sources."""
        return self._sources

    @sources.setter
    def sources(self, sources) -> None:
        """Set sources."""
        self._sources = self._type_list(sources)

    @property
    def includes(self) -> typing.List[str]:
        """Return includes."""
        return self._includes

    @includes.setter
    def includes(self, includes) -> None:
        """Set includes."""
        self._includes = self._type_list(includes)

    @property
    def libraries(self) -> typing.List[str]:
        """Return libraries."""
        return self._libraries

    @libraries.setter
    def libraries(self, libraries) -> None:
        """Set libraries."""
        self._libraries = self._type_list(libraries)

    @property
    def outputs(self) -> typing.List[str]:
        """Return outputs file."""
        return self._outputs

    @outputs.setter
    def outputs(self, outputs) -> None:
        """Set outputs."""
        self._outputs = self._type_list(outputs)

    @property
    def output(self) -> str:
        """Return output file."""
        return self._outputs[0]

    @output.setter
    def output(self, output) -> None:
        """Set output."""
        self._outputs[0] = self._type_string(output)
