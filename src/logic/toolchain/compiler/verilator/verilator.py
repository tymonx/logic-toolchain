#!/usr/bin/env python3
#
# Copyright 2019 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Verilator compiler."""

import os
import copy
import subprocess

from .. import Compiler
from .. import Arguments


@Compiler.register()
class Verilator(Compiler):
    """Verilator compiler."""

    @staticmethod
    def compose_arguments(arguments: Arguments) -> Arguments:
        """Compose arguments."""
        arguments = copy.copy(arguments)
        sources = copy.copy(arguments.sources)
        arguments.sources = []

        for source in sources:
            if not os.path.isabs(source):
                source = os.path.join(arguments.working_directory, source)
                source = os.path.abspath(source)

            extension = os.path.splitext(source)[1]

            if extension in '.json':
                arguments.extend(
                    Verilator.compose_arguments(Arguments(source))
                )
            elif extension in ['.v', '.sv']:
                arguments.sources.append(source)
            elif extension in ['.h', '.vh', '.svh']:
                arguments.includes.append(os.path.splitext(source)[0])

        return arguments.unique()

    def archive(self, arguments: Arguments) -> None:
        """Create a static library."""
        verilator = self._compiler
        working_directory = arguments.working_directory
        library_dir = os.path.dirname(arguments.output)
        library_base = os.path.splitext(arguments.output)[0]
        library_name = os.path.basename(library_base)

        if library_dir:
            working_directory = library_dir

        if library_name.startswith('lib') and arguments.output.endswith('.a'):
            module_name = library_name[3:]
        else:
            module_name = library_name

        if not verilator.endswith('_bin'):
            verilator += '_bin'

        command = [
            verilator,
            '--sc',
            '-CFLAGS', '-fPIC',
            '-Wall',
            '--coverage',
            '--Mdir', library_name,
            '--prefix', module_name
        ]

        args = Verilator.compose_arguments(arguments)

        command.extend(args.options)

        for define in args.defines:
            command.extend(['-D', define])

        for path in args.paths:
            command.extend(['-y', path])

        for include in args.includes:
            command.extend(['-I', include])

        command.extend(args.sources)

        print(' '.join(command))

        subprocess.run(
            command,
            cwd=working_directory,
            check=True
        )

        command = [
            'make',
            '-C', library_name,
            '-f', module_name + '.mk'
        ]

        print(' '.join(command))

        subprocess.run(
            command,
            cwd=working_directory,
            check=True
        )

        arguments.dumb(library_base + '.json')

        if os.path.lexists(library_base + '.a'):
            os.remove(library_base + '.a')

        os.symlink(
            os.path.join(library_name, module_name + '__ALL.a'),
            library_base + '.a'
        )

    def library(self, arguments: Arguments) -> None:
        """Create a shared library."""
        working_directory = arguments.working_directory
        library_dir = os.path.dirname(arguments.output)
        library_base = os.path.splitext(arguments.output)[0]
        library_name = os.path.basename(library_base)

        if library_dir:
            working_directory = library_dir

        arguments.output = library_base + '.a'

        self.archive(arguments)

        arguments.output = library_base + '.so'

        command = [
            'gcc',
            '-shared',
            '-o',
            library_name + '.so',
            '-Wl,--whole-archive',
            library_name + '.a',
            '-Wl,--no-whole-archive'
        ]

        print(' '.join(command))

        subprocess.run(
            command,
            cwd=working_directory,
            check=True
        )

        arguments.dumb(library_base + '.json')
