#!/usr/bin/env python3
#
# Copyright 2019 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Compiler base module."""

import os
from typing import Dict, Type
from .arguments import Arguments


class Compiler:
    """Compiler base class."""

    __entities: Dict[str, Type['Compiler']] = {}

    def __init__(self, compiler: str, compiler_id: str):
        """Constructor."""
        self._id = compiler_id
        self._compiler = compiler

    @classmethod
    def register(cls):
        """Register class to factory."""
        def decorator(subclass):
            """Register method decorator."""
            compiler_id: str = subclass.__name__.lower()

            if compiler_id not in cls.__entities:
                cls.__entities[compiler_id] = subclass
            else:
                raise ValueError('Compiler name is not unique: ' +
                                 subclass.__name__)
            return subclass
        return decorator

    @classmethod
    def create(cls, compiler: str):
        """Create object based on registered class."""
        if not isinstance(compiler, str):
            raise TypeError('Argument must be a string type not a ' +
                            type(compiler))

        name: str = os.path.splitext(os.path.basename(compiler))[0].lower()

        try:
            entity = cls.__entities[name]
            return entity(compiler, name)
        except KeyError:
            for compiler_id, entity in cls.__entities.items():
                if compiler_id in name:
                    return entity(compiler, compiler_id)

        raise ValueError('Cannot find compiler ' + compiler)

    def get_id(self) -> str:
        """Return compiler id."""
        return self._id

    @classmethod
    def get_entities(cls) -> Dict[str, Type['Compiler']]:
        """Return compiler entities."""
        return cls.__entities

    def compile(self, arguments: Arguments) -> None:
        """Create an object file."""
        raise RuntimeError("Cannot create an object file")

    def archive(self, arguments: Arguments) -> None:
        """Create an archive library."""
        raise RuntimeError("Cannot create an archive library")

    def library(self, arguments: Arguments) -> None:
        """Create a shared library."""
        raise RuntimeError("Cannot create a shared library")

    def link(self, arguments: Arguments) -> None:
        """Create an executable file."""
        raise RuntimeError("Cannot create an executable file")

    def drive(self, arguments: Arguments) -> None:
        """Drive compiler."""
        if not arguments.output:
            arguments.output = "a.json"

        extension = os.path.splitext(arguments.output)[1]

        if extension in '.json':
            arguments.dumb(arguments.output)
        elif extension in ['.o', '.obj']:
            self.compile(arguments)
        elif extension in ['.a', '.lib']:
            self.archive(arguments)
        elif extension in ['.so', '.dll']:
            self.library(arguments)
        elif extension in ['', '.exe']:
            self.link(arguments)
        else:
            raise ValueError('Unknown output file extension')

    @staticmethod
    def make_unique(items):
        """Make unique."""
        unique_items = []

        for item in items:
            if item not in unique_items:
                unique_items.append(item)

        return unique_items


def create(compiler: str):
    """Create compiler."""
    return Compiler.create(compiler)
